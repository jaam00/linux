install: (sudo apt-get install | pacman -S) xbindkeys. then:

touch ~/.xbindkeysrc
xbindkeys -k; {hit a key [comb]; e.g. ctr-alt-h will display}

"NoCommand"
    m:0xc + c:43
    Control+Alt + h

cp the output to ~/.xbindkeysrc, adding a cmd and commenting out the second or third line, e.g.

#    Control+Alt + h
"sudo vbetool dpms off"
    m:0xc + c:43

#    Control+Alt + otilde
"sudo shutdown -h now"
    m:0xc + c:35

reload the configuration file and apply the changes:

xbindkeys -p

to make changes permanent, add to (~/.(profile | xinitrc {before the line starting your wm or de}):

xbindkeys

https://wiki.archlinux.org/index.php/Xbindkeys
https://askubuntu.com/questions/913080/how-to-rebind-ctrlshiftq-to-using-xbindkeys {answ}
