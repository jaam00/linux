atril (pdf viewer)
npm
node (check folders to rm: find /usr -maxdepth 3 -type d -printf "%p %TY-%Tm-%Td %TH:%TM:%TS %Tz\n"|grep "21-12-11 11:4") 
python3.8 (inf: arvuti/pl/python/py38_install.txt; loc: /usr/local/{lib,bin,include,share/man})
enchant (spell checker)

note that if you want to rm e.g. xfburn (xfce cd burner), its meta package xfce4-goodies will be removed as well, making the following autoremovable:

ristretto xfce4-battery-plugin xfce4-clipman xfce4-clipman-plugin
  xfce4-cpufreq-plugin xfce4-cpugraph-plugin xfce4-datetime-plugin xfce4-dict
  xfce4-diskperf-plugin xfce4-fsguard-plugin xfce4-genmon-plugin
  xfce4-mailwatch-plugin xfce4-netload-plugin xfce4-notes xfce4-notes-plugin
  xfce4-places-plugin xfce4-screenshooter xfce4-sensors-plugin
  xfce4-smartbookmark-plugin xfce4-systemload-plugin xfce4-taskmanager
  xfce4-timer-plugin xfce4-verve-plugin xfce4-wavelan-plugin
  xfce4-weather-plugin xfce4-whiskermenu-plugin xfce4-xkb-plugin

2 ways to go about this:

1. Just remove xfce4-goodies, note which packages are suddenly auto-removable, and then mark desired packages from that list as manual†. When it's done, autoremove the rest
2. Remove xfce4-goodies and then reinstall the individual packages you actually use that are bundled within the meta package

UPSHOT: be careful w/ autoremove if you use meta pack-s!

† https://manpages.ubuntu.com/manpages/trusty/man8/apt-mark.8.html

https://askubuntu.com/questions/720239/apt-get-autoremove-dangerous/1025327#1025327
https://itectec.com/ubuntu/ubuntu-does-removing-a-metapackage-not-remove-the-dependencies-which-it-installed/
https://www.reddit.com/r/debian/comments/amedim/trying_to_remove_ristretto_without_removing/efoimgs?utm_source=share&utm_medium=web2x&context=3
https://www.reddit.com/r/debian/comments/amedim/trying_to_remove_ristretto_without_removing/eflljwr?utm_source=share&utm_medium=web2x&context=3

--uninstalled:--

arduino ide (which arduino)
visual studio code (usually installed from deb: https://code.visualstudio.com/docs/setup/linux)
mog (likely uninstalled)
coursera-dl
udeler (udemy course dl-er)
ipython3
remmina
virtualenv (rev: sudo pip3.8 install virtualenv : sudo pip3.8 uninstall virtualenv)
venv (inf: ~/bin/py38; loc: git/bb)
~/.config/Code (visual studio code, should be uninstalled)
~/.config/Notepadqq
~/.local/bin
~/.local/lib
~/.local/share/virtualenv