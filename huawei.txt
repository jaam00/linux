-------huawei e3372 lte (emt/telia):--------

storage:
DefaultVendor=0x12d1
DefaultProduct=0x1f01

modem:
TargetVendor=  0x12d1
TargetProduct= 0x14dc

https://unix.stackexchange.com/a/362811/123256

profile name: EMT(Default)
pin disabled

pin: 0000
pin2: 1111
puk: 26301724
puk2: 41270161

http://192.168.8.1/html/home.html

http://www.draisberghof.de/usb_modeswitch/bb/viewtopic.php?t=2414

--t-shoot:--

proble: lsusb shows 12d1:157d
reason: 12d1:157d will not be switched to a new id if your system contains the cdc_mbim driver, it will remain with its initial id but usb_modeswitch will select its config 2 which is an mbim configuration. You can see from your logs that this has happened so NetworkManager (if not too old) should now be able to pick up the wwan ethernet device created by the cdc_mbim driver. usb_modeswitch will switch the device into 12d1:14db/14dc if the cdc_mbim driver is absent or if the /etc/usb_modeswitch.conf file contains the line "DisableMBIMGlobal=1"

http://www.draisberghof.de/usb_modeswitch/bb/viewtopic.php?p=15840&#p15840
