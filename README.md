# linux

## ./
- [ ] my notes, instructions and cheat sheets on various linux matters

## bin/
- [ ] bash scripts

## cnf/
- [ ] configurations of my various distros and systems

## inst/
- [ ] detailed installation instructions for distros

## contributing
if you want to comment or point out errors, please [email me](mailto:erkkil@gmail.com) or raise an [issue](https://gitlab.com/jaam00/linux/-/issues)

## license
[three-claused BSD License](https://opensource.org/licenses/BSD-3-Clause)