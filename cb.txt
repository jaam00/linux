alt-space : show active window menu

--changes:--

connect to gsm : r-click - connection manager icon > edit conn-s : mobile broadband : add..; then activate as per stc
terminal : xset m 28 2
/.config/terminator/config : ﻿﻿font = Monospace 10 --> 12 OR --terminal:--
settings > tint2 > edit config file : panel_position = top center horizontal --> bottom center horizontal
settings > user interface settings : default font : liberation sans 10 --> 12
settings > openbox > gui config tool > (appearance : fonts : (all) liberation sans 10 --> 12 | mouse : change dbl click time)

minimize all windows:

Edit your rc.xml file and look for the section that begins like "<!-- Keybindings for windows -->" then add this:

    <keybind key="A-F3">
      <action name="Iconify"/>
    </keybind>

Save the file and the make sure to go to Openbox menu > Preferences > Openbox Config > Reconfigure.

--geany:--

edit > pref-s > (editor > line wrap [tick] / terminal : font / interface : font)

--terminal:--

r-click in terminal > pref-s > prof-s > (general : font / background : solid)

--live:--

user: crunchbang
passwd: live

--cb-welcome:--

http://crunchbang.org/forums/viewtopic.php?id=24113

y to all (except for dev tools)

--scr-saver:--

settings > screensaver (change|disable)

--problems:--

screen garbled (blinking lines that persist even after rebooting to another distro) in both 32 and 64-bit ver-s. seemingly related to big programs w/ large win-s like lo and gimp (e.g. not terminal and file man)
