The  snap  command lets you install, configure, refresh and remove snaps. Snaps are packages that work across many different Linux distributions, enabling secure delivery and operation of the latest apps and utilities.

snap (find|install|remove|info|refresh|...) pack
snap help [info|...]
snap list

man snap

note: if you install your 1st snap, you get the core snap dl-ed, installed, and parts of it possibly mounted as loop dev-s. if you rm all your snaps, the core w/ its dev-s remains. rm it w/:

sudo apt purge snapd

https://askubuntu.com/questions/1000177/is-the-snap-core-folder-needed/1000200#1000200

--t-shoot:--

proble: doesn't seem to work (w/ e.g. notepadqq (can't open symlinks, folder contents hidden, etc.)
solu: use a ppa pack instead (see notepadqq.txt)

https://askubuntu.com/questions/1009698/permission-for-snap-applications