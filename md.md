newline : append >=2 spaces to a line and hit <enter>
quote : prepend >=2 spaces to a line (works in wiki)
next-to-invisible line : ##  <enter>
italic : _txt_ OR *txt*
bold : **txt**
tab : tab
codeblock : >=2 tabs
inline code : ` what `
inline code (displaying backticks) : `` `what`  ``
key : <kbd>down</kbd>
strikeout : ~~strikeout~~ OR
~~strike
out~~ OR <s>strikeout</s>
comment : [//]: # (a comment)
continue a numbered list disrupted by a code block : use four spaces to indent content between list items:
# nlc0 # : 1st level title (toplevel, largest)
## nlc0 ## : 2nd level title
... : the next level titles come w/out the line separator
--- : line separator (like ________________________________________________________________)

1. item 1
    ```
    Code block
    ```
2. item 2

note: do not indent 3-backtick codeblock markers for a multiline codeblock (as it won't preserve lines)

```
multiline codeblock

```

this will disrupt numbering though. to preserve the numbering, rm the backticks and use 8-space indentation for the code block (this will drop syntax hilighting though)

note: you may use ~~~, etc. instead of 3 backticks (but always use a fencing style in pairs)

https://stackoverflow.com/questions/15212168/mimic-a-newline-in-markdown/32810451#32810451
https://stackoverflow.com/questions/22385334/how-to-write-one-new-line-in-bitbucket-markdown/56845923#56845923
https://stackoverflow.com/questions/35948953/display-a-back-tick-within-back-ticks#35948986
https://stackoverflow.com/questions/4823468/comments-in-markdown
https://stackoverflow.com/questions/18088955/markdown-continue-numbered-list
https://webapps.stackexchange.com/questions/14986/strikethrough-with-github-markdown/103691#103691
https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html
https://spec.commonmark.org/0.29/

----ex:----

The [issue](https://github.com/coq/coq/issues/9899#issuecomment-479613062) is that CoqIDE's default restart/compile to end shortcuts mask Gnome/Xfce/..'s go to beginning/end of file shorcuts. As I'm used to e.g. <kbd>Ctr</kbd>+<kbd>&rarr;</kbd>, I was specifically interested in changing only the two masking shortcuts <kbd>Ctr</kbd>+<kbd>Home</kbd> and <kbd>Ctr</kbd>+<kbd>End</kbd>. Achieved this by setting

```
(gtk_accel_path "<Actions>/Navigation/End" "<Alt>End")
(gtk_accel_path "<Actions>/Navigation/Start" "<Alt>Home")
```
in `~/.config/coq/coqide.keys` (notice the removed `;`), and commenting out

`# modifier_for_navigation = "<Control>" `

in `~/.config/coq/coqiderc`, and making both files read-only

https://meta.askubuntu.com/questions/7352/how-do-i-add-arrows-in-my-post/7383#7383