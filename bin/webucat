#!/usr/bin/env bash
# format webucator manuals for e-reader
# prepare .txt w/: pdftotext -layout <pdf-file>

### FUNCTIONS ###
printAddr(){																					#DEBUG
	echo "addr-s of $1 blocks w/ surplus spaces:"
	[ ${#begln[@]} -ge ${#endln[@]} ] && greater=${#begln[@]} || greater=${#endln[@]}
	for ((c=0; c<$greater; c++)); do
		echo "[ ${begln[c]}, ${endln[c]} ]"
	done
	echo "total: ${#begln[@]}"
	echo "$jsi" >jsi_error.txt
}

readArr(){ 
	readarray -t begln < <(echo "$jsi"| grep -no ¤| tr -d '¤:') 								#block beg ln no-s
	readarray -t endln < <(echo "$jsi"| grep -no Ł| tr -d 'Ł:') 								#block end ln no-s
	[ ${#begln[@]} -eq ${#endln[@]} ] || { { echo "error: the number of block beg and end markers doesn't match"; printAddr $1; }| tee error_msg.txt; exit 1; }
}

changeLog(){ 
	[ $c -gt 0 ] && { echo "------------------${begln[c-1]}, ${endln[c-1]}:--------------------" >> changelog.txt
	[ -s changes.txt ] && { cat changes.txt >> changelog.txt; rm changes.txt; } || echo UNCHANGED >> changelog.txt; }
}

truncateGenericBlock(){ 
	for ((c=0; c<${#begln[@]}; c++)); do
		changeLog
		while true; do
			[[ $(echo "$jsi"| sed -rn "${begln[c]},${endln[c]}{/^Ł?¤? [[:graph:]]/p}") ]] && break || jsi="$(echo "$jsi"| sed -r "${begln[c]},${endln[c]}s/^(Ł?¤?) /\1/w changes.txt")"
		done
	done
}

truncateBlockCleanup(){ 
rm changes.txt
echo "$jsi"| sed -r 's/(¤|Ł)//g' >jsi.txt																	#rm code block markers, write to file
}

#### FILE ####
[[ $2 ]] && [[ -f $1 ]] || { echo "webucat ——— format webucator manuals (*.txt) for e-reader" ; echo "usage: webucat input.txt output.txt"; exit 1; }
cp "$1" jsi.txt

### BREAKS ###
sed -i 's/\f//g' jsi.txt																							#rm form feed (\x0c)
sed -i -rz 's/\n +(LESSON [0-9]{1,3})\n +([a-zA-Z])/\n\n\1: \2/g' jsi.txt											#lesson title layout
sed -i -rz 's/ *(([0-9]+ \| )?LESSON [0-9]+: [[:graph:]][[:print:]]+( \| [0-9]+)?) */------\1------\n\n/g' jsi.txt	#format pg break

#### TOC ####
echo "toc.."
sed -i -r 's/  +(Table of Contents)/\n\n\1\n/' jsi.txt														#format toc title
#format toc
readarray -t toc < <(grep "\.\.\.\." jsi.txt) 																#array of toc items
readarray -t len < <(grep "\.\.\.\." jsi.txt| sed -r 's|\.{2,}||g'| awk '{print length}') 					#array of lengths of toc items (- pad and 0c)
for ((c=0; c<${#len[@]}; c++)); do 																			#cycle through toc items
	[ "${len[$c]}" -gt 48 ] && offset=110 || offset=50
	padlen=$((offset-${len[$c]}))
	pad=$(printf "%-${padlen}s"| tr " " ".")
	sed -i "s|${toc[$c]}|\a${toc[$c]%%....*}${pad}${toc[$c]##*....}|" jsi.txt								#format and protect toc
done

#### HEADERS ####
echo "headers.."
sed -i -rz 's/(ples and)\n *(hands-on)/\1 \2/' jsi.txt														#flow title
sed -i -r 's/ *❋/\t\t\t\t\t❋/g' jsi.txt
sed -i '/Topics Covered/d' jsi.txt																			#rm Topics Covered line
sed -i -rz 's/\nIntroduction\n/\n\nIntroduction\n/g' jsi.txt												#add \n bef Introduction
sed -i -rz 's/\n\n(([[:print:]]+\n)+\n {8,}– [[:print:]]+)\n/\n\n¤\1/g' jsi.txt								#set motto beg marker
#format motto (from beg to end marker)
sed -i -r '/¤/{:a; / {8,}– /{s/([A-Za-z])\n {8,}([A-Za-z])/\1 \2/g; s/(^|\n)¤? {8,}/\1     /g; b}; N; ba}' jsi.txt

#### TABLES ####
echo "tables.."
#format and protect "Text  Explanation|Validator  Description" tables
sed -i -r '/ *(Text  +Explanation|Validator  +Description) *$/{:a; N; /\n\n/{s/([[:graph:]]) *\n  +([[:graph:]])/\1 \2/g; /[[:graph:]] {20,}[[:graph:]]/s/([[:graph:]]) {18}/\1/g; s/([[:print:]]+[[:graph:]]+  +[[:graph:]])/\a\1/g; b}; ba}' jsi.txt
sed -i -rz 's/([[:graph:]]) *\n\a( *)(Text|Validator)(  +)(Explanation|Description) *\n/\1\n\n\a\3\2 \4\5\n\n/' jsi.txt

# format and protect (Operator-Name-Example|Value-State-Description) tables. 1. format title; 2. format content; 3. format table end marker
sed -i -r '/(Operator +Name +Example|Value +State +Description)/{:a; /\n\n\n?[A-Z\n]/{s/(Operator|Value) +(Name|State) +(Example|Description)/\n\a\1\t\2\t\t\t\3\n/g;         s/\n([[:punct:][:digit:]]+)  +([A-Z][[:print:]]+[[:graph:]])  +([[:graph:]][[:print:]]+[[:graph:]])  +([[:print:]]+)/\n\a\1\t\t\2\t\t\t\3 \4/g; s/\n([[:punct:][:digit:]]+)(  +|\t\t+)([A-Z][[:print:]]+[[:graph:]])(  +|\t\t+)(([[:graph:]]|\$)[[:print:]]+)/\n\a\1\t\t\3\t\t\t\5/g; s/\n([[:punct:]]+\n)/\n\a\1/; s/\n {50,}([^ \n\t\a][[:print:]]+\n)/ \1/g; s/\n\n\n/\n/g;        /\n(Operator|Value)\t/!s/(\n[A-Z])/\n\1/; b}; N; ba}' jsi.txt

#protect some horiz tables (end marker "^ *Word | ----")
sed -i -r '/(ty {3,25}|tor {3,44}|tate {3,44})Desc/{:a; /\n( {,9}[A-Z][a-z]+ | +----)/{s/\n/\a\n/g; b}; N; ba}' jsi.txt
sed -i -r 's/^([0-9]+ +% )/\a\1/g' jsi.txt																	#protect modulus tables

# format and protect interleaved tables. s-cmd-s: 1. format title row; 2. format row A—B—C; 3. format and flow continuation —Bx—Cx; 4. format and flow continuation —Bx; 5. flow continuation ——Cx; 6. protect table rows
sed -i -r '/lass   +Represents/{:a; N; /\n\n+ *[A-Z]/{s/ *(Pseudo-class) +(Represents) +(Can be applied to)/\1\t\t\2\t\t\3\n/g;         s/ *(:[a-z-]+)  +([A-Z][^\n]+[^ ]) +([A-Z][^\n]+)/\1\t\t\2\t\t\3/g;         s/(:[a-z-]+\t\t)([A-Z][^\n\.]+[^ ])\t\t([A-Z][^\n\.]+)\n  +([a-z(]+[^\n]+[^ \n])  +([a-z(]+[^\n]+)/\1\2 \4\t\t\3 \5/g;          s/(:[a-z-]+\t\t[A-Z][^\n\t\.]+)\t\t([A-Z][^\n]+)\n  {,30}([a-z(]+[^\n]+)/\1 \3\t\t\2/g;          s/(:[a-z-]+\t\t[A-Z][^\n]+\t\t[A-Z][^\n\.]+)\n  {50,}([a-z(]+[^\n]+)/\1 \2/g;          s/\n/\a\n/g; b}; ba}' jsi.txt

# convert some horiz tables to vertical
sed -i -r "/(od {3,}|ty {26,}|or {36,})D/{:a; /\n( {,9}[A-Z][a-z]+ | +----)/{s/(\n[a-z][a-zA-Z\.\(]+,) {3,}([^\n ]+[^\n]+)\n([a-zA-Z]+\))( ?| {3,}([^\n ]+[^\n]+))\n/\a\1 \3\n\n\2\4/g;          s/\n([a-zA-Z][a-zA-Z\.]+(\(([a-zA-Z]+(, [a-zA-Z]+)*)?\))?) {3,}([^\n ][^\n;/\[]+)/\n\a\1\n\n\5/g;           s/\n([[:punct:]]+) {3,}([^\n ]+[^\n]+)/\n\1\n\n\2\n/g;           s/\n    +([^\n]+(;| \/\/|[a-z]\(')[^\n]*)/\n\a   \1/g;          s/([^\n]+[^\n ])    +([^\n]+(;| \/\/|[a-z]\(')[^\n]*)/\1\n\a   \2/g;           s/\n([^\n]+)\n\n+(\a   [^\n]+(;| \/\/|[a-z]\(')[^\n]*)/\n\1\n\2/g;          s/([^\n])\n\a([a-zA-Z][a-zA-Z\.]+(\(([a-zA-Z]+(, [a-zA-Z]+)*)?\))?)/\1\n\n\a\2/g; b}; N; ba}" jsi.txt
# convert some horiz tables to vertical. s-commands in inner {...} (starting from the first): 1. fix interleaved "formula.part1 description.part1 formula.part2 description.part2" and convert to vert; 2. convert regular horiz "formula description" to vert; 3. convert horiz "punctuation-formula description" to vert; 4. format regular code ex-s in description; 5. put code ex-s (in description) not on a separate line to a separate line and format them; 6. rm surplus \n-s between code ex-s; 7. put 2 \n-s before a formula if it has one

#WARNING: this may rm table captions
#sed -i -rz 's/\n *[[:print:]]+ +Description */\n/g' jsi.txt													#rm "x ... Desc"

#### CODE (ALL) ####
echo "code: generic.."
sed -i -rz 's/\n\n *([^0-9][^\n]+; *)\n\n/\n\n \1\a\n\n/g' jsi.txt										#js: format and protect separate codeline
sed -i -rz "s/\n\n *(<[^\n]+)\n\n/\n\n \1\a\n\n/g" jsi.txt												#html: format and protect separate codeline
sed -i '/ \/\*/{:a; / \*\//{N; s/\n/\a\n/g; b}; N; ba}' jsi.txt												#protect contents of /* ... */ w/ \a
sed -i '/<!--/{:a; /-->/{N; s/\n/\a\n/g; b}; N; ba}' jsi.txt												#protect contents of <!-- ... -->
sed -i -r '/<p(>| )/{:a; /<\/p>/{N; s/\n/\a\n/g; b}; N; ba}' jsi.txt										#protect contents of <p ... /p>

#protect enum-ed code block (append \a to each line)
sed -i -r "/^[0-9]{1,3}\. +((<|\/\/|\?|\\\$?[a-zA-Z]+ *=|INSERT|VALUE|JOIN|COUNT|WHERE|INTERVAL|SELECT|FROM|GROUP|ORDER|LIMIT|[a-z]\.[a-z]|\([a-z])[[:print:]]+|[[:print:]]+(\{|\}|;|':|>|[a-z]+_[a-z]+\))) *$/{:a; /\n\n/{s/\n/\a\n/g; b}; N; ba}" jsi.txt

sed -i -r 's/((Solution: |Demo [1-9]|Exercise Code: )[[:print:]]+)/\1\a/g' jsi.txt							#protect enum-ed code header
sed -i -rz 's/\n( *([[:alpha:] -]{2,50}(:|\?)) *)\n([^\n]*(\{|;|<)[^\n]*)/\n\1\n\n\4/g' jsi.txt				#format non-enum-ed code header

sed -i -r 's/([[:graph:]]) {2,}([[:graph:]][[:print:]]+;) *$/\1 \2/g' jsi.txt								#js: rm surplus " " in "x {2,}x"
sed -i -r 's/^ *(<[[:print:]]+[[:graph:]]) {2,}([[:graph:]])/\1 \2/g' jsi.txt								#html: rm surplus " " in "x {2,}x"
sed -i -r 's/([[:alnum:]])  +(\{|↵↵)/\1 \2/g' jsi.txt														#code: rm surplus " " in "x  +(\{|↵↵)"

#protect (only) non-enum-ed codelines. note the non-subst cond† in the beg (relevant for † below). TODO: sep codeline end and beg?
sed -i -r '/(^ *[0-9][0-9\.][0-9\. ]|[a-z)]\. [A-Z]|>\.? [A-Z]?[a-z ]{20,})/!s/^( *(\/\/|<|\[|if ?\([A-Za-z]+|(function )?[A-Z\.a-z]+\(|const [A-Za-z]+|[[:alnum:]]+ = |"[[:alnum:]]|\\\$?[a-zA-Z]+ *=|INSERT|VALUE|JOIN|WHERE|INTERVAL|SELECT|FROM|GROUP|ORDER|LIMIT)[[:print:]]*|[[:print:]]*(>(…|,)?|;|\+|\]|↵↵|---|\{…?|\},?|(\x27|")(:|\)|,)?|[a-z]+_[a-z]+\)|\(\)|[a-z] ?\(|\}<\/[a-z]+>\.|(^|\}| |, [0-9]+)\),?) *)$/\1\a/g' jsi.txt

#flow non-enum-ed codeline betw enum-ed ones
sed -i -rz 's/([0-9]+\. +[^\n]+\a)\n +([^\n]+(>|;)\a\n)([0-9]+\. +[^\n]+\a)/\1\2\4/g' jsi.txt

#### NON-ENUM-ED CODE BLOCKS ####
#mark non-enum-ed code blocks w/ "^  +" w/ block beg (¤) and end (Ł) markers
jsi="$(sed -rz 's/\n\n(  +(\{|\/\/|\x27?<|[^0-9][^0-9\.][^\.][^\n]+(\{|↵↵) *\a?\n|(function |ReactDOM\.)?[A-Za-z]+\(|(const|import|return) |let [A-Za-z]+|\)))/\n\n¤\1/g' jsi.txt)"
jsi="$(echo "$jsi"| sed -r '/¤/{:a; N; /\n(\n| *---)/{s/([[:print:]]+\a?\n(\n| *---))/Ł\1/; b}; ba}')"
readArr "non-enum-ed code"
rm changelog.txt
echo "CODE BLOCK CHANGELOG" >> changelog.txt
echo "++++++++++++++++++NON-ENUM-ED CODE:++++++++++++++++++" >> changelog.txt
truncateGenericBlock
truncateBlockCleanup

#### NON-ENUM-ED OUTLINES & LISTS ####
echo "outlines/lists.."
#format and protect non-enum-ed outline (schemes|file lists), adding block beg and end markers
sed -i -r '/following (outline|files)/{:a; /(\n{3,}|\n{2,}  +[0-9A-Zxiv]+\. )/{s/:\n( {8,}[A-Z][^\.])/:\n\n¤\1/; s/([A-Za-z])\n\n( {8,}[A-Z][^\.])/\1\a\n\2/g; s/\n( +[A-Za-z\.]+)\n\n/\nŁ\1\a\n\n/; b}; N; ba}' jsi.txt

#format and protect " {6,}[a-z-]+: [^\.]+" lists
sed -i -rz 's/\n([A-Z][[:print:]]+:|[[:print:]]+----\a)\n+( {6,}[a-z-]+: [^\.]+)/\n\1\n\n¤\2/g' jsi.txt
sed -i -r '/¤ {6,}[a-z-]+: [^\.]+/{:a; N; /\n{2,}([A-Z][a-z]+|----)/{s/([a-z:]) {2,}([[:graph:]])/\1 \2/g; s/\.\n/\.\a\n/g; s/([[:graph:]])\n {6,}([[:graph:]])/\1 \2/g; s/\n( {6,}[a-z-]+: [[:print:]]+\a)\n{2,}([A-Z][a-z]+|----)/\nŁ\1\n\n\2/; b}; ba}' jsi.txt

jsi="$(< jsi.txt)"
readArr "non-enum-ed outline/list"
#printAddr "non-enum-ed outline/list"				#DEBUG
echo "++++++++++++++++++NON-ENUM-ED OUTLINE/LIST:++++++++++++++++++" >> changelog.txt
truncateGenericBlock
truncateBlockCleanup

#### PROCESS & PROTECT ####
echo "protecting.."
#sed -i -r '/LESSON [0-9]{1,3}: /{:a; /Introduction/{s/\n/\a\n/g; b}; N; ba}' jsi.txt						#protect contents of LESSON ... Introduction
#WARNING: not all manuals have the "LESSON ... Introduction" structure. comment out by default
sed -i -rz 's/\n( {,7}([0-9\.]+ )?[a-zA-Z][^\n\a]{3,60})\n( {,7}[a-zA-Z1-9][^\n\a]+)/\n\1\n\n\3/g' jsi.txt	#add \n after subtitle
sed -i -rz 's/(\n *Method [A-Z0-9]+ *)\n( *[^\n])/\1\n\n\2/g' jsi.txt										#add \n after subtitle
sed -i -r '/\a/!s/^( +)?([1-9]+\.) ( +)?([^ ])/\2 \4/g' jsi.txt												#rm "[ ]+"-s in unprot enumerations
sed -i -r '/[^\a]/{/^ *[A-J]\.  */{s/ {2,}/ /g; s/ ?([A-J]\.)/\a  \1/g}}' jsi.txt							#format and protect capital enumerations
sed -i -r '/[^\a]/{/^ +([iv]+\.)  */{s/ {2,}/ /g; s/ ?([iv]+\.) /\a    \1 /g}}' jsi.txt						#format and protect lowercase roman enum-s
sed -i -r '/[^\a]/{/^ *[a-h]\.  */{s/ {2,}/ /g; s/ ?([a-h]\.)/\a      \1/g}}' jsi.txt						#format and protect lowercase enumerations
sed -i -r 's/^ *(console.(log|dir)\(.*\);?) *$/ \1\a/g' jsi.txt												#format and protect "console.x()"† lines
sed -i -rz 's/\a{2,}/\a/g' jsi.txt																			#cleanup (\a sanity check)
sed -i -rz 's/\n\a\n/\n\n/g' jsi.txt																		#cleanup (\a sanity check)
sed -i -rz 's/([^\n])\n( *([1-9][0-9]? to [1-9][0-9]? minutes) *)/\1 (\3)\n/g' jsi.txt						#format exercise header

#### ENUM-ED CODE BLOCKS ####
echo "code: numbered.."
#mark code blocks w/ "[0-9]+\.  +"
jsi="$(sed -rz 's/((---|\n\n[0-9]+\.|[^\n]*(Demo|Solution:)[^\n]*)\a?|\n\a?)\n([0-9]+\.  )/\1\n¤\4/g' jsi.txt)" 	#jsi.txt w/ code block beg marker ¤
jsi="$(echo "$jsi"| sed -r '/¤/{:a; N; /\n(\n| *---)/{s/([[:print:]]+\a?\n(\n| *---))/Ł\1/; b}; ba}')" 		#jsi.txt w/ code block beg and end marker Ł
readArr "enum-ed code"

#rm surplus " " in "[0-9]+\.  +[[:graph:]]" in enum-ed code blocks (w/ debug info)
echo "++++++++++++++++++ENUM-ED CODE:++++++++++++++++++" >> changelog.txt
for ((c=0; c<${#begln[@]}; c++)); do
	changeLog
	while true; do
		[[ $(echo "$jsi"| sed -rn "${begln[c]},${endln[c]}{/[0-9]+\. [[:graph:]]/p}") ]] && break || jsi="$(echo "$jsi"| sed -r "${begln[c]},${endln[c]}s/([0-9]+\.) /\1/w changes.txt")"
	done
done
truncateBlockCleanup

#### POSTPROCESS & FLOW ####
echo "cleanup"
#WARNING: matches false positives (e.g. x(y - z)):
sed -i -r 's/^( *[a-zA-Z\(\)\.\$_]+) +(->?) +([^\n]+)/\1 \2 \3\a/g' jsi.txt									#protect "formula - text" lists
sed -i -rz 's/(:)\n(\a[a-zA-Z\(\)\.\$_]+ ->? )/\1\n\n\2/g' jsi.txt											#format "formula - text" lists header
sed -i -rz 's/(\a[a-zA-Z\(\)\.\$_]+ ->? [^\n]+)\n( *[A-Z][a-z]+)/\1\n\n\2/g' jsi.txt						#format "formula - text" lists footer
sed -i -rz 's/([^\n\a]{2,})\n([^\n\a 1-9+/*=%\-])/\1 \2/g' jsi.txt											#flow lines w/ regular (non-enum) txt

#WARNING: may match false positives:
sed -i -rz 's/([a-z \.,-])\n([0-9][0-9]*[^\.]{2,})/\1 \2/g' jsi.txt							               	#flow lines accidentally starting w/ numbers
sed -i -rz 's/([^\n\a]{2,})\n {2,}([^\n\a 1-9])/\1 \2/g' jsi.txt											#flow regular txt lines starting w/ " "-s
sed -i -r '/\a/!s/^ +//g' jsi.txt																			#rm " "+ from the beg of unprot reg lines

sed -i -r '/\a/!s/ {2,}/ /g' jsi.txt																		#rm redundant " "-s in unprotected lines
sed -i -r 's/^( *([0-9]+|[xiv]+|[A-H]+)\. [[:print:]]+[[:graph:]]) {2,}([[:graph:]])/\1 \3/g' jsi.txt		#rm inner "  +"-s in enum-s
sed -i -rz 's/(\n[0-9]+\. [^\n\a]+[^:;\a])\n\n+([0-9]+\. [^\n\a])/\1\n\2/g' jsi.txt							#\n\n+ --> \n in unprot enum-s
sed -i -rz 's/(\n[0-9]+\. [^\n\a]+[^:;\a])\n\n+([0-9]+\. [^\n\a])/\1\n\2/g' jsi.txt							#\n\n+ --> \n in unprot enum-s
sed -i -rz 's/(\n[\a ]*[xiv]+\. [^\n]+)\n\n+([\a ]*[xiv]+\. [^\n])/\1\n\2/g' jsi.txt						#\n\n+ --> \n in roman enum-s
sed -i -rz 's/(\n[\a ]*[xiv]+\. [^\n]+)\n\n+([\a ]*[xiv]+\. [^\n])/\1\n\2/g' jsi.txt						#\n\n+ --> \n in roman enum-s
sed -i -rz 's/(\n[\a ]*[a-hA-H]\. [^\n]+)\n\n+([\a ]*[a-hA-H]\. [^\n])/\1\n\2/g' jsi.txt					#\n\n+ --> \n in alpha enum-s
sed -i -rz 's/(\n[\a ]*[a-hA-H]\. [^\n]+)\n\n+([\a ]*[a-hA-H]\. [^\n])/\1\n\2/g' jsi.txt					#\n\n+ --> \n in alpha enum-s
sed -i -rz 's/(\n[\a ]*([a-hA-H]|[xiv]+)\. [^\n]+\n\n)\n+([\a ]*[0-9]+\. [^\n])/\1\3/g' jsi.txt				#\n\n\n+ --> \n\n in subenum/enum transitions
sed -i -rz 's/([1-9][0-9]?\. [^\n]+[^:])\n{3,}([0-9]+\. )/\1\n\n\2/g' jsi.txt								#reduce triple+ \n-s betw enum-s to double

sed -i 's/  <?/ <?/g' jsi.txt																				#fix php layout
sed -i -rz '{:a; s/([1-9]\. [a-z]+\(\))\n\n([1-9]\. )/\1\n\2/g; ta}' jsi.txt								#format enum-ed date(), time().. seq
sed -i -rz '{:a; s/([1-9]\. E_[A-Z]+: [^\n]+)\n\n([1-9]\. )/\1\n\2/g; ta}' jsi.txt							#format enum-ed E_* seq
sed -i -rz 's/(\n[^\n\a1-9][^\n]+[^\n\a])\n([^\n\aa-zA-Z][0-9]*\. [^\n]+)/\1\n\n\2/g' jsi.txt				#add \n betw regular line followed by enum
sed -i -r 's/(Glob|De|getboot) (al_|mos|strap)/\1\2/g' jsi.txt												#correct http address
sed -i -r 's/([a-z]+-) ([a-z-]+\.(html|js|php))/\1\2/g' jsi.txt												#correct path address
sed -i -rz 's/( (Methods|Properties))\n{4}/\1\n\n\n/g' jsi.txt												#rm redundant \n-s 
sed -i -rz 's/( Operators)\n{3}/\1\n\n/g' jsi.txt															#rm redundant \n-s
sed -i -rz 's/\n{3,}/\n\n\n/g' jsi.txt																		#rm (the rest of) redundant \n-s
sed -i 's/\a//g' jsi.txt

#### FILE ####
mv jsi.txt "$2"