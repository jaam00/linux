----basic:----

xkbcomp $DISPLAY output.xkb : get xkb map
xkbcomp input.xkb $DISPLAY : set xkb map
xev -e keyboard : get (physical) keycode to keysym mappings
setxkbmap -(query|print) [-v 10] : query map infos [verbose]

https://wiki.archlinux.org/index.php/X_keyboard_extension#Getting_and_setting_XKB_layout
https://unix.stackexchange.com/questions/49650/how-to-get-keycodes-for-xmodmap/49651#49651
man setxkbmap

--nonrecomm-ed:--

sudo loadkeys j.map : load keymap to console
sudo dumpkeys [-l] > k.map : [long info]
sudo showkey : get key bindings

note: at least in x these seem to be useless/obsolete
note: showkey's keycode = (usually) xev's code -8

http://www.tldp.org/HOWTO/Keyboard-and-Console-HOWTO-15.html

------jelly comb:------

- to make use of the "ins := fn-bs" def-ed below, you must switch to win mode w/ fn-e
- in win mode, F1 := fn-1, ..., F12 := fn-beta (the top row)
- F11 switches betw/ full screen; F12 gives ~ (esc char) in term, in micro you can map it to an action (see micro.txt)
- to access blue func-s (speaker, etc.), switch to android mode w/ fn-w
- in android mode *w/out Group2 active*, F1 := alt-1, ..., F12 := alt-beta (the top row)

-------.xkb map:-------

xkb map can have any name/extension; C++ style comments, // till the end of line, are allowed. syntax:

<INS> = 119;							//<label> = keycode
key <RWIN> { [ ISO_Next_Group ] };		//key <label> { [ keysym ] };

//modifiers:					none			shf			altgr			altgr-shf:
..........
symbols[Group1]= [               1,          exclam,     onesuperior,      exclamdown ]

    interpret Alt_L+AnyOf(all) {							//interpret Alt_L+... 
        virtualModifier= Alt;
        action= SetMods(modifiers=modMapMods,clearLocks);	//as a normal modifier (left alt must be pressed when pressing ...)
    };

    interpret Alt_L+AnyOf(all) {							//interpret Alt_L+... 
        virtualModifier= Alt;
        action= LockMods(modifiers=modMapMods);				//as a toggle (left alt is "on" till its next press)
    };

note: a comprehensive list of keysym-s is in X's source headers (the main one is /usr/include/X11/keysymdef.h), obtainable by installing x11proto-core-dev. there're also online lists of /usr/include/X11/keysymdef.h (2 first links below):

https://cgit.freedesktop.org/xorg/proto/x11proto/tree/keysymdef.h
https://android.googlesource.com/platform/prebuilts/gcc/linux-x86/host/x86_64-linux-glibc2.7-4.6/+/02075080d51c371ae87b9898bf84a085e436ee27/sysroot/usr/include/X11/keysymdef.h
https://wiki.archlinux.org/index.php/X_keyboard_extension#Editing_the_layout
https://wiki.archlinux.org/index.php/X_keyboard_extension#Using_keymap
https://wiki.archlinux.org/index.php/X_keyboard_extension#Switching_a_single_modifier_bit
https://www.x.org/releases/current/doc/libX11/XKB/xkblib.html
https://askubuntu.com/questions/93772/where-do-i-find-a-list-of-all-x-keysyms-these-days/93789#93789
/usr/share/X11/xkb[/rules]
/usr/include/X11[/keysymdef.h]
https://www.tcl.tk/man/tcl8.4/TkCmd/keysyms.htm

----remapped:----

key			value 
++++++++++++++++
` 			del
del			ins
altgr-'		`
^			esc
...

----edited:----

xkb_keycodes "evdev+aliases(qwerty)" {
...
    <INS> = 119;
    <DELE> = 21;
    <ESC> = 49;
};

xkb_compatibility "complete" {
...
    indicator "Caps Lock" {
...
        groups= 0xfe;	//make caps lock show group 2
    };

note: get the group (mask) code from

    indicator "Group 2" {
        !allowExplicit;
        groups= 0xfe; };

below. we cycle betw Group 1/2 w/ ISO_Next_Group (triggered w/ RWIN below)

xkb_symbols "pc+ee(nodeadkeys)+inet(evdev)+terminate(ctrl_alt_bksp)" {

    name[group1]="Estonian (no dead keys)";	//estonian layout group
...
    key <BKSL> {
        type= "FOUR_LEVEL",
        symbols[Group1]= [      apostrophe,        asterisk,         grave,       breve ]
    };
...
        symbols[Group1]= [               1,          exclam,      asciitilde,      exclamdown ]
        symbols[Group1]= [               2,        quotedbl,              at,       onesuperior ]
        symbols[Group1]= [               3,      numbersign,        asciicircum,        sterling ]
        symbols[Group1]= [               4,        currency,          dollar,          yen ]
        symbols[Group1]= [               5,         percent,         asciicircum,    onehalf ]
        symbols[Group1]= [               6,       ampersand,         notsign,     oneeighth ]
        symbols[Group1]= [               w,               W,         U2020,         U2021 ]
        symbols[Group1]= [               n,               N,               dead_belowdot,        abovedot ]
        symbols[Group1]= [           minus,      underscore,   bar,        U2014 ]
        symbols[Group1]= [               x,               X,       greater,       guillemotright ]
        symbols[Group1]= [               c,               C,            bar,       copyright ]
...
        key <RWIN> { [ ISO_Next_Group ] };	//group switch	
	    key  <ESC> {
    	    type[Group2]= "FOUR_LEVEL",
			symbols[Group2]= [              Escape,             1,            2,           3 ]
		};
        key <AE01> {						//F1...F10 group
            type[Group2] = "CTRL+ALT",
            symbols[Group2] = [             F1,              F1,              exclam,      asciitilde, XF86Switch_VT_1 ]
        };
        key <AE02> {
            type[Group2] = "CTRL+ALT",
            symbols[Group2] = [             F2,              F2,              quotedbl,              at, XF86Switch_VT_2 ]
        };
        key <AE03> {
            type[Group2] = "CTRL+ALT",
            symbols[Group2] = [             F3,              F3,              numbersign,        asciicircum, XF86Switch_VT_3 ]
        };
        key <AE04> {
            type[Group2] = "CTRL+ALT",
            symbols[Group2] = [             F4,              F4,              currency,          dollar, XF86Switch_VT_4 ]
        };
        key <AE05> {
            type[Group2] = "CTRL+ALT",
            symbols[Group2] = [             F5,              F5,              percent,         asciicircum, XF86Switch_VT_5 ]
        };
        key <AE06> {
            type[Group2] = "CTRL+ALT",
            symbols[Group2] = [             F6,              F6,              ampersand,         notsign, XF86Switch_VT_6 ]
        };
        key <AE07> {
            type[Group2] = "CTRL+ALT",
            symbols[Group2] = [             F7,              F7,              slash,       braceleft, XF86Switch_VT_7 ]
        };
        key <AE08> {
            type[Group2] = "CTRL+ALT",
            symbols[Group2] = [             F8,              F8,              parenleft,     bracketleft, XF86Switch_VT_8 ]
        };
        key <AE09> {
            type[Group2] = "CTRL+ALT",
            symbols[Group2] = [             F9,              F9,              parenright,    bracketright, XF86Switch_VT_9 ]
        };
        key <AE10> {
            type[Group2] = "CTRL+ALT",
            symbols[Group2] = [             F10,             F10,              equal,      braceright, XF86Switch_VT_10 ]
        };
};

note: the edit was done for jelly comb

--dell 7280:--

        symbols[Group1]= [           minus,      underscore,   dead_belowdot,        abovedot ]
-->
        symbols[Group1]= [           minus,      underscore,   emdash,     figdash ]

https://wiki.archlinux.org/index.php/X_keyboard_extension#Indicators
https://superuser.com/questions/801611/how-to-make-all-applications-respect-my-modified-xkb-layout/844673#844673
https://unix.stackexchange.com/questions/153841/how-to-make-altgri-j-k-l-work-properly-as-cursor-keys/155984#155984
https://unicode-table.com/en/blocks/general-punctuation/
https://stackoverflow.com/questions/39315057/xkb-three-key-shortcut-to-acyclic-switch-keyboart-layout-like-in-windows

---permanent:---

on xubuntu at least you should create ~/.xsessionrc:

#intercept jelly comb after default set-s have been set
{ sleep 15 && [[ $(lsusb|grep "258a:6a88") ]] && test -f /home/j/.xkb && xkbcomp /home/j/.xkb $DISPLAY; } >/dev/null 2>&1 &

note: use full folder paths in this file, not ~
note: the file should be executable by everyone

https://superuser.com/questions/597291/xfce-lightdm-startup-configuration-files/687401#687401
https://bbs.archlinux.org/viewtopic.php?pid=1723504#p1723504
https://unix.stackexchange.com/questions/439905/how-do-i-get-my-custom-keyboard-layout-to-show-up-in-the-xfce4-keyboard-settings

-------------------background:-------------------

instructions on how to fix F1-12 etc, in linux 
By David Top Contributor: Sewing on June 24, 2015

This keyboard has a very nice feel, and works well with both my laptop and android phone. First off, let me answer a question I had, which I couldn't answer until I actually got the device: this keyboard does NOT have the ability to pair with multiple devices at once. So if you want to use it with multiple devices, you will have to repair every time you switch.
It has a nice feel. Keys are slightly reduced in size from normal, but it's not much of a problem. I have fairly large hands and it took me a little time to get used to it; but after a few minutes I was able to type almost as well as with regular keyboard. I did a typing test online, and got about 65 wpm with both. I have gotten the occasional key repeat. Maybe it doesn't feel like keybounce--keyboard feels very solid. And it's pretty rare, so it might just be that I had a slight hitch in my keystroke or something. Anyway, Seems pretty good there.
Battery life seems good, but as other reviewers have said there is no battery indicator. I've been using off and on for a couple of weeks, almost always with the red backlight on, and it's still going strong on my first charge.
Now about the layout: there are some major gripes here. Some can be fixed, one can't. All of these apply to using with a computer, not a phone or tablet. As you can see from photos, it has an F1-12 key row. By default they are media keys. Using the Fn+QWE keys switches between iOS, Android and Windows modes, which changes the keycodes which are sent by these keys. According to the instructions, in windows mode when you press Fn+F1-12, you are supposed to get the F1-12 key. Doesn't happen. At least not on my unit.
Other problems: I know there are no real "standards" with keyboard layout but seems like most have the ctrl key at far bottom left, and the Fn key just to its right. This can't be fixed, and it pretty annoying.
Also this keyboard has the windows keys immediately on either side of the space bar, and the alt key is just to the left of the left window key. Again, standard for most keyboards is to have the alt keys next to space, and windows key in between left ctrl and alt.

At least on Linux (which is what I'm using) all of these problems except for the Fn/Ctrl thing can be fixed. Here's how:
First, run xinput to figure out the id number for the jelly comb keyboard. You ought to get something like this:

↳ Bluetooth 3.0 Keyboard id=14 [slave keyboard (3)]

The thing we want is the "14". Now we are going to get the current mapping, using this command:

xkbcomp -i 14 $DISPLAY xkb.dump

When you run this command, make sure the number following the -i is the number you got from xinput up above!
This will "dump" your current keyboard layout into a file called xkb.dump. Open this file in a text editor.
We are interested in the top section, which is probably titled:

xkb_keycodes "evdev+aliases(qwerty)" {
...
};

It has a listing of all of the key names, and their mapped key codes. Now the right way to do this is to go through and find and alter all of these key codes, and then remove the lines that have the "wrong" mappings. I just stuck my replacement remaps at the bottom. I get warning messages when I use it, but it recognizes that there are multiple mappings with the same keycode and just uses the last one it comes to.
Okay, so here are the keycodes I used. I'm guessing these will always be the same...but if for some reason it doesn't work you can get the correct keycodes using either xev or showkey (warning, with showkey, you have to add 8 to the code it gives you. I don't know why, you just do. But showkey will give results that you won't get sometimes with xev, because at least for me the screen brightness F1/F2 get grabbed by xwindows before they get to xev, and so the keycodes never even show up in xev). Again, here's what I used. I put them right at the end of this section, before the };

<FK01> = 232;
<FK02> = 233;
<FK03> = 128;
<FK04> = 212;
<FK05> = 237;
<FK06> = 238;
<FK07> = 173;
<FK08> = 172;
<FK09> = 171;
<FK10> = 121;
<FK11> = 122;
<FK12> = 123;
<LALT> = 133;
<RALT> = 134;
<LWIN> = 64;

This will remap the F1-12 keys so that they are always F1-12 (you won't get the media keys at all), and it also swaps the alt and win keys (since they are backwards from every other keyboard in the universe).
Once this is done, save the dump file with the changes, and then run this command:
xkbcomp -i 14 xkb.dump $DISPLAY
Again, make sure you substitute the id number you got from xinput in place of the 14. Also if you renamed the xkb.dump to something else, you need to change the file name here.
And there you have it, now the keyboard should be mapped properly! If something goes wrong, run this command to restore the maps back to default:
setxkbmap -device 14 us
This should restore you back to the default US layout. Again substitute your xinput id for the 14.
BTW I typed this whole review using external keyboard.

https://www.amazon.com/Bluetooth-Keyboard-Jelly-Comb-Rechargeable/dp/B01LS32D52#customerReviews (srch: linux)