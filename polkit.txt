The actions available to you via polkit will depend on the packages you have installed. Some are used in multiple desktop environments (org.freedesktop.*), some are DE-specific (org.gnome.*) and some are specific to a single program (org.archlinux.pkexec.gparted.policy). The command pkaction lists all the actions defined in /usr/share/polkit-1/actions for quick reference.

To get an idea of what polkit can do, here are a few commonly used groups of actions:

- systemd-logind (org.freedesktop.login1.policy) actions regulated by polkit include powering off, rebooting, suspending and hibernating the system, including when other users may still be logged in.
- udisks (org.freedesktop.udisks2.policy) actions regulated by polkit include mounting file systems and unlocking encrypted devices.
- NetworkManager (org.freedesktop.NetworkManager.policy) actions regulated by polkit include turning on and off the network, wifi or mobile broadband.

Each action is defined in an <action> tag in a .policy file. The org.archlinux.pkexec.gparted.policy contains a single action and looks like this:

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE policyconfig PUBLIC
 "-//freedesktop//DTD PolicyKit Policy Configuration 1.0//EN"
 "http://www.freedesktop.org/software/polkit/policyconfig-1.dtd">
<policyconfig>

  <action id="org.archlinux.pkexec.gparted">
    <message>Authentication is required to run the GParted Partition Editor</message>
    <icon_name>gparted</icon_name>
    <defaults>
      <allow_any>auth_admin</allow_any>
      <allow_inactive>auth_admin</allow_inactive>
      <allow_active>auth_admin</allow_active>
    </defaults>
    <annotate key="org.freedesktop.policykit.exec.path">/usr/bin/gparted</annotate>
    <annotate key="org.freedesktop.policykit.exec.allow_gui">true</annotate>
  </action>

</policyconfig>
The attribute id is the actual command sent to D-Bus, the message tag is the explanation to the user when authentication is required and the icon_name is sort of obvious.

The defaults tag is where the permissions or lack thereof are located. It contains three settings: allow_any, allow_inactive, and allow_active. Inactive sessions are generally remote sessions (SSH, VNC, etc.) whereas active sessions are logged directly into the machine on a TTY or an X display. allow_any is the setting encompassing both scenarios.

For each of these settings the following options are available:

no: The user is not authorized to carry out the action. There is therefore no need for authentication.
yes: The user is authorized to carry out the action without any authentication.
auth_self: Authentication is required but the user need not be an administrative user.
auth_admin: Authentication as an administrative user is required.
auth_self_keep: The same as auth_self but, like sudo, the authorization lasts a few minutes.
auth_admin_keep: The same as auth_admin but, like sudo, the authorization lasts a few minutes.
These are default setting and unless overruled in later configuration will be valid for all users.

As can be seen from the GParted action, users are required to authenticate as administrators in order to use GParted, regardless of whether the session is active or inactive.

Authorization rules
Authorization rules that overrule the default settings are laid out in a set of directories as described above. For all purposes relating to personal configuration of a single system, only /etc/polkit-1/rules.d should be used.

The addRule() method is used for adding a function that may be called whenever an authorization check for action and subject is performed. Functions are called in the order they have been added until one of the functions returns a value. Hence, to add an authorization rule that is processed before other rules, put it in a file in /etc/polkit-1/rules.d with a name that sorts before other rules files, for example 00-early-checks.rules.

The layout of the .rules files is fairly self-explanatory:

/* Allow users in admin group to run GParted without authentication */
polkit.addRule(function(action, subject) {
    if (action.id == "org.archlinux.pkexec.gparted" &&
        subject.isInGroup("admin")) {
        return polkit.Result.YES;
    }
});
Inside the function, we check for the specified action ID (org.archlinux.pkexec.gparted) and for the user's group (admin), then return a value "yes".

https://wiki.archlinux.org/title/Polkit#Actions