http://www.willus.com/k2pdfopt/help/options.shtml
http://www.willus.com/k2pdfopt/help/linux.shtml

k2pdfopt -p 83-84 Saibi_-_1999_-_Outils.pdf
k2p
k2p -mr 0.5
-m[r] 0.5 : crop [right] margins† 0.5in¹
-h 680 : set output dev height to 680 pix ((kindle's) default is 735)

some other opt-s i've tried:

-dpi
-dr 2
-vb -2
-vs 0.5

note: you tend to get the best result w/ default set-s (i.e. no opt-s except perhaps -p...)
† "all margins" includes also top and bottom
¹ this was useful for rm-ing the graphic "part" indicator in java reference, which messed up adjacent text size