https://atom.io/docs/latest/

--config:--

†disable spell check: ctr-, > packages > spell check > disable
†disable language-text {lorem, lipsum; cf. autocomplete}: ctr-, > packages > language-text > disable
†hide vertical line in editor: ctr-, > packages > wrap guide > disable
†disable autocomplete-plus: ctr-, > packages > autocomplete-plus : show suggest-s on keystroke [untick]
install language-latex: (ctr-, > install : language-latex | apm install language-latex)
untick "show welcome guide.." (in "welcome" window) 
se(e|lect) grammar: bottom right (on status bar)

hide treeview: init.coffee :
atom.packages.onDidActivateInitialPackages ->
  workspaceView = atom.views.getView(atom.workspace);
  atom.commands.dispatch(workspaceView, 'tree-view:toggle');

note: if treeview is displayed, run the init script *once* to hide it, then uncomment
note 0: toggle treeview {by default; cf. keybindings} : ctr-(k,b)

editor settings: config.cson {note the identation}:
".latex,.plain":
	editor:
		softWrap: true
		softWrapAtPreferredLineLength: true
		preferredLineLength: 80
		fontSize: 28

add a class for single quoted text: ~/.atom/packages/language-latex/grammars/latex.cson :
  {
    'begin': '`'
    'beginCaptures':
      '0':
        'name': 'punctuation.definition.string.begin.latex'
    'end': '\''
    'endCaptures':
      '0':
        'name': 'punctuation.definition.string.end.latex'
    'name': 'string.quoted.single.latex'
    'patterns': [
      {
        'include': '$base'
      }
    ]
  }

† also in config.cson
http://rolflekang.com/writing-latex-in-atom/

--package:--

atom has its own package man (apm)

install new package|theme : (apm install ... | ctr-, > install : ...)
list installed packages: apm list
uninstall package: apm uninstall ...

if you install|generate a package|theme, it goes under ~/.atom/packages
warning: use uninstall; rm-ing (package|theme) by deleting it will leave a lot to clean up. once i was able to get rid of a generated theme after deleting it under ~/.atom/(github&&packages) only by : undelete; treeview : r-click : rm project folder

to downgrade a pack, uninstall the existing one, go to https://atom.io/packages, find the pack, click on versions, sel your ver, dl it, decompress the tar.gz or .zip to ~/.atom/packages/<yourpack> (for clarity, you can include the ver name in <yourpack>)

note: "apm install <pack>" installs the latest ver from internet

https://discuss.atom.io/t/manually-install-package/9251/27

--autocomplete:--

view available (autocomplete) snippets (along w/ triggers, in "trigger/snippet" format): alt-shf-s
disable plain txt autocomplete snippets (legal, lorem) for all scopes: ctr-, > packages > language-text {disable}; restart atom
disable all autocomplete for plain txt only: settings > packages > autocomplete-plus > settings > scope blacklist : .text.plain {cf. ‡ for a list of scopes}
tweak autocomplete keybindings: settings > packages > autocomplete-plus > settings (e.g. w/ "use strict.." and "minimum word length")

note: legal and lorem are under language-text/snippets/language-text.cson in https://github.com/atom/language-text/blob/master/snippets/language-text.cson

show existing "snippets" on sys (except those of gedit): fnd *snippets* | grep -v gedit

https://discuss.atom.io/t/how-can-i-see-what-triggers-a-snippet/15043/6
http://stackoverflow.com/questions/25989997/how-to-turn-off-snippets-in-atom/26060489#26060489
‡ https://github.com/execjosh/atom-file-types
cf. /usr/share/atom/resources/app/apm/templates/language/snippets/language-__package-name__.cson

--keybindings:--

ctr-(up|down) moves current line (up|down)
navigation is chromium-like (ctr-tab, ctr-w, etc.)
ctr-(+|-): make txt (larger|smaller)
command palette: ctr-shf-p

change keybinding: ctr-, > keybindings > cp {icon to row's left}; paste to ~/.atom/keymap.cson; change binding (note that non-us letters don't work as parts of keycomb-s; press ctr-. to check for available comb-s and their values)
keybinding-resolver (shows k-comb's value and where it's defined (useful if you want to change it)): ctr-.

--regex:--

the old ver uses $<num> instead of \<num> for pattern recall in regex replace. ex:

([a-zA-Z])\[
$1 [

https://github.com/atom/find-and-replace/issues/351

--theme:--

‡ui: one dark
‡syntax: (atom dark|solarized light†|zenbow-warmↁ)

†~/.atom/styles.less :
atom-text-editor {
   color: #333333;
}

open dev tools (ctr-shf-i) to inspect an ele, then change a value of its class in ~/.atom/styles.less (e.g. use gcolor2 to change colors):

ↁ --styles.less:--

//zenbow-warm tweaks

atom-text-editor {
  //3F3F39 -->
  background-color: #3C3C35;
}

atom-text-editor.editor .syntax--variable.syntax--parameter.syntax--function.syntax--latex {color: #97B34B}
atom-text-editor.editor .syntax--variable.syntax--parameter.syntax--latex {color: #B797C0}
atom-text-editor.editor .syntax--reference.syntax--citation.syntax--latex {color: #78AEA4}
atom-text-editor.editor .syntax--math.syntax--tex, atom-text-editor.editor .syntax--string.syntax--tex {color: #FFC124}
atom-text-editor.editor .syntax--string.syntax--latex, atom-text-editor.editor .syntax--quoted.syntax--latex {color: #799C57}
atom-text-editor.editor .syntax--text {color: #FFFACF}

------

#6D5973 : tumelilla
#B797C0 : helelilla
#FFC124 : orans
#FFDA24 : kollane
#97B34B : roheline
#799C57 : tumeroheline
#579C90 : teal (sinakasroheline, meresinine)
#78AEA4 : teal-hall
#FFF5CA|FFFACA|+FFFACF|FFF8CA|FFF8C6|F5EEBE|E6E2D0 : valkjaskollane
#3B3B34|+3C3C35 : tumehall

another example:

@atom-ui-font-size: 10px;

html, body, .tree-view, .status-bar, .tab-bar .tab {
  font-size: @atom-ui-font-size;
}

more themes: darkula-syntax{make txt color lighter?}, zenburn{+}, quantum-syntax{?}, oceanic-next{?}, mbo, spacegray-black-syntax{?}, railscasts-theme{?}, green-syntax{make txt color lighter}, zenbow-(warm{make yellow yellower}|cool)-syntax{+}, beacon-ui{?}

‡ also in config.cson
https://atom.io/docs/latest/hacking-atom-creating-a-theme
http://webdesign.tutsplus.com/tutorials/how-to-create-a-custom-theme-for-atom--cms-20878
http://enrmarc.github.io/atom-theme-gallery/
https://discuss.atom.io/t/adjust-color-of-brackets-used-in-shortcodes/13679/2

--t-shoot:--

-language-latex:-

ver 1.0 doesn't display txt correctly w/ my atom ver 1.2.3, had to downgrade (to 0.6.1)

-stylesheet:-

problem: 'stylesheet' persists in treeview after edit > open your stylesheet. 2 solu-s:
1. cursor on 'stylesheet' in treeview > context menu {r-click} > remove project folder
2. atom <path>

https://discuss.atom.io/t/cannot-open-your-stylesheet-in-linux/21840/8
https://github.com/atom/atom/issues/9767

-language-text snippets:-

Q: What does the package `language-text` exactly do besides firing snippets `lorem` and `legal`? I understood it has something to do with grammar (or rather bulleting). I'm asking this because I had to disable the package to block only `lorem` and `legal` (didn't find another way to do it -- a long story and separate topic, took me a day to look into).

A: It is the definition of the Plain Text grammar, which is used as the fallback if no other grammar is recognized. You can find the grammar rules for it here: https://github.com/atom/language-text/blob/master/grammars/plain%20text.cson

https://discuss.atom.io/t/uses-of-language-text/21831

--obs:--

How to block the two language-text snippets

I want to disable only the two default autocomplete snippets `lorem` and `legal`. From [github][1] I know they are in package `language-text`, in file `language-text.cson`. The package is installed by default but I don't have this file on my Linux system. I tried to block the snippets as suggested on [SO][2] but received

> Cannot find module '/usr/share/atom/resources/app.asar/node_modules/snippets'

I have no such folder (`app.asar` is a huge (likely packed) file and `app.asar.unpacked` has no `snippets` under `node_modules`). Grepping around didn't produce any useful results. Following [the advice][3], I looked up the `language-text` package to click on the `View Code` button... which I don't have (neither with the package enabled nor disabled). This seems to be a [bug][4] (albeit a more severe one than described there). Any advice on how to proceed with disabling only the two snippets?

  [1]: https://github.com/atom/language-text/blob/f85efb1c4740768df3536df63afe04836105a563/snippets/language-text.cson
  [2]: http://stackoverflow.com/a/26060489/4967226
  [3]: https://discuss.atom.io/t/how-can-i-see-what-triggers-a-snippet/15043/6
  [4]: https://github.com/atom/settings-view/issues/617
