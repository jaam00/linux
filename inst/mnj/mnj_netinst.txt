﻿manjaro net install:

Tip: If possible, ensure that you are connected to the internet prior to booting from your installation media (e.g. disc, USB flash drive, or even an ISO file directly if booting in Oracle's Virtualbox). If you have a hard-wired connection via an ethernet cable, then Manjaro will automatically connect to the internet without you having to do anything. Otherwise, once you have started the installation process, you will be able to manually connect to your wireless network. The Stable Installer section of the User Guide for Experienced Users provides further information about this.

First, set your preferred language by pressing the F2
Second, set your preferred keyboard layout (keymap) by pressing the F3
Tip: Don't worry if your preferred language or keyboard layout isn't listed, as a far wider range of languages can be selected during the installation process itself, if necessary.
For the best results, select the 'Boot Manjaro Linux with non-free graphics drivers'
Log into Manjaro. both the username and passwords are manjaro.
Tip: If you wish to connect to the internet using a wireless connection, now is the time to do it!
To start the actual installation process, enter the command: sudo setup
choose stable installer and complete all steps *in their order*
Once you have re-booted and logged into your newly installed system, update it: sudo pacman -Syu
install a dm (lxdm): sudo pacman -S lxdm
enable LXDM in Manjaro >=0.8.2 (or if Plymouth has been removed): sudo systemctl enable lxdm.service -f
enable LXDM in Manjaro =<0.8.1 (or if Plymouth has been added): sudo systemctl enable lxdm-plymouth.service -f
install Openbox: sudo pacman -S openbox
install a logout script, configuration application, menu-editor, and extra themes specifically for Openbox: sudo pacman -S oblogout obconf obmenu openbox-themes

https://wiki.manjaro.org/index.php?title=Installation_Guide_for_the_NET_Edition_0.8.2
https://wiki.manjaro.org/index.php?title=Install_Desktop_Environments