#!/bin/sh
alias shtd='pkill x &
dhcpcd -x $(ip link | grep -o "ww[[:alnum:]]\+: <" | tr -d ": <"); umount /root/a /root/b; shutdown now'
alias rbt='pkill x &
dhcpcd -k $(ip link | grep -o "ww[[:alnum:]]\+: <" | tr -d ": <"); reboot'
alias mkpkg='sudo -u nobody makepkg -sri'
alias l='ls -lcotApr'
alias fnd="find / -name $1"
alias c='cat \"$1\" | less'
alias chrom=startx
acpi
bl
