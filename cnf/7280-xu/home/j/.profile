# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

# increase trackpad sensitivity (was 30 2)
xset m 4444 1

#disable all wireless dev-s w/ a delay, run on the bg (optionally also done in ~/bin/stc)
{ sleep 6; sudo rfkill block all; } >/dev/null 2>&1 &

#needed for mc
EDITOR="mousepad 2>/dev/null"; export EDITOR

bash -c ". /home/j/bin/stc" >/dev/null 2>&1 &	#autoconn to emt gsm
