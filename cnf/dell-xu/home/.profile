# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

xset m 4444 1

#---power save options:---

if [ "$(cat /sys/class/power_supply/AC/online)"=0 ]; then
	bash -c "/home/j/bin/pow t"
else
	bash -c "/home/j/bin/pow f"
fi

#---nm client options:---

xbindkeys
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/NX/lib
sudo vbetool dpms off
sed -i 's/"Window state" value="normal"/"Window state" value="fullscreen"/' ~/.nx/config/player.cfg
#setxkbmap -model evdev layout -ee
