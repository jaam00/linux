# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin directories
PATH="$HOME/bin:$HOME/.local/bin:$PATH"

################ MY CONFIG: ##################

EDITOR=/bin/nano -$; export EDITOR

#power save options:
if [ "$(cat /sys/class/power_supply/ACAD/online)"=0 ]; then
	bash -c "/home/j/bin/pow t"
else
	bash -c "/home/j/bin/pow f"
fi

#disable all wireless dev-s w/ a delay, run on the bg (optionally also done in ~/bin/stc)
{ sleep 6; sudo rfkill block all; } >/dev/null 2>&1 &
if [ -e /home/j/.nix-profile/etc/profile.d/nix.sh ]; then . /home/j/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
