# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi
#export PATH		#not req-ed if/as the variable is already in the environment

# increase trackpad sensitivity (was 30 2)
{ sleep 6; xset m 66666 1; } >/dev/null 2>&1 &
#{ sleep 6; xset m 6666 1; } >/dev/null 2>&1 &

#disable all wireless dev-s w/ a delay, run on the bg (optionally also done in ~/bin/stc)
{ sleep 6; sudo rfkill block wifi; } >/dev/null 2>&1 &
#sleep 6 && sudo rfkill block wifi

#conn gsm, start db
#bash -c ". /home/j/bin/stc" >/dev/null 2>&1 &

#enable the useful s-cut
setxkbmap -option "terminate:ctrl_alt_bksp"

#load custom keymap
xkbcomp /home/j/7280/home-j/d7280.xkb $DISPLAY

#: << 'NM'
#nomachine autoconn
#sed -i 's/"Window state" value="normal"/"Window state" value="fullscreen"/' ~/.nx/config/player.cfg
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/NX/lib
sleep 9 && xrandr --output LVDS-1 --off &
#sudo mount -o remount,exec /dev && sleep 3; sudo vbetool dpms off
/usr/NX/bin/nxplayer --session "/home/j/NoMachine/db 169.254.142.141.nxs"
#NM
