# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

#disable all wireless dev-s w/ a delay, run on the bg (optionally also done in ~/bin/stc)
#{ sleep 6; sudo rfkill block wifi; } >/dev/null 2>&1 &

#conn gsm, start db
#{ dropbox stop; bash -c ". /home/j/bin/stc"; dbus-launch dropbox start; } >/dev/null 2>&1 &
#{ dropbox stop; dbus-launch dropbox start; } >/dev/null 2>&1 &

# increase trackpad sensitivity (was 30 2)
#{ sleep 6; xset m 6666 1; } >/dev/null 2>&1 &
#xset m 6666 1
xset m 66666 1

#for nomachine
setxkbmap -model evdev -layout ee

#enable the useful s-cut
setxkbmap -option "terminate:ctrl_alt_bksp"

#load custom keymap
#{ echo "~/.xsessionrc last line" > ~/login.log; xkbcomp /home/j/d7280.xkb $DISPLAY; } >/dev/null 2>&1 &
xkbcomp /home/j/d7280.xkb $DISPLAY

if [ -e /home/j/.nix-profile/etc/profile.d/nix.sh ]; then . /home/j/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
