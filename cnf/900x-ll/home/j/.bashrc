# enable bash completion in interactive shells
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

alias usage='du -sk * | sort -n | perl -ne '\''($s,$f)=split(m{\t});for (qw(K M G)) {if($s<1024) {printf("%.1f",$s);print "$_\t$f"; last};$s=$s/1024}'\'
alias ls="ls --color"

#----my stuff:----

#aliases
alias mc='export LD_PRELOAD=/usr/local/lib/libtrash.so.3.3; . /usr/lib/mc/mc-wrapper.sh /mnt/DATA/Dropbox/tekst/arvuti/linux'
alias ci='/home/j/.nix-profile/bin/coqide'
alias zo='/opt/zotero/zotero'
alias cdk='cd /mnt/DATA/Dropbox/tekst'
alias cdo='cd /mnt/DATA/Dropbox/tekst/teooria'
alias cdl='cd /mnt/DATA/Dropbox/tekst/arvuti/linux'
alias cdd='cd /mnt/DATA/dl'
alias cdn='cd /nix/var/nix/profiles'
alias lsl='ls -lctAr'
alias k2p='k2pdfopt'
alias g='gedit 2>/dev/null'
alias hex='hexedit --color'
alias sudo='sudo ' #A trailing space in value causes the next word to be checked for alias substitution when the alias is expanded [man bash]

#fun-s
dug(){ sudo du -h "$1" | grep '[0-9\,]\+G'; }

