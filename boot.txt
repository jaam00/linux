you may use boot-repair ("recommended repair") to get the sys it's being invoked from to the top of grub menu. this is also possible by configuring grub

https://help.ubuntu.com/community/Boot-Repair
https://linuxnorth.wordpress.com/2011/03/09/grub2-revisited/

--ex result of a recomm-ed rep:--

Boot successfully repaired.

Please write on a paper the following URL:
http://paste.ubuntu.com/14393022/


In case you still experience boot problem, indicate this URL to:
boot.repair@gmail.com or to your favorite support forum.

You can now reboot your computer.


The boot files of [The OS now in use - Ubuntu 14.04.1 LTS] are far from the start of the disk. Your BIOS may not detect them. You may want to retry after creating a /boot partition (EXT4, >200MB, start of the disk). This can be performed via tools such as gParted. Then select this partition via the [Separate /boot partition:] option of [Boot Repair]. (https://help.ubuntu.com/community/BootPartition)