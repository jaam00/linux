automount partition (DATA) on startup:
get the UUID (Universally Unique Identifier) of the partition you want to mount: sudo blkid
create a mount point: sudo mkdir /mnt/DATA
gksu gedit /etc/fstab and add the following line in the fstab file (replace the UUID with the one you got earlier):
# mount DATA (/dev/sda2)
# UUID=01CEBA167194FF40 /mnt/DATA ntfs rw,nosuid,uid=1000,nodev,noatime,allow_other 0 0
# mount DATA (/dev/sda4)
UUID=3fb116d2-7177-45f7-8d6e-36d6a5325638 /mnt/DATA ext4 [rw,suid,exec,dev,]noatime 0 0

alt: you can get a template of what your fstab line for a dev might look like from /etc/mtab (the info (options?) there may be dated, however)
note: the default options for ext4 file system are: rw,suid,dev,exec,auto,nouser,async,relatime

--automount external dev-s:--

noauto,nofail,x-systemd.automount,x-systemd.device-timeout=1ms,...

https://askubuntu.com/questions/161544/how-does-the-fstab-defaults-option-work-is-relatime-recommended
https://wiki.archlinux.org/index.php/Fstab#Usage
https://anonexp.blogspot.com/2013/04/ext4-mount-options-for-ext4-file-system.html
https://wiki.archlinux.org/index.php/Fstab#External_devices

----t-shoot:----

-inaccessible mount points / (external) fs-s':-

possible problems: writing requires root privilege, ownership changes to root, even root cannot change ownership, mount point busy even w/ nth mounted, etc.

solu (w/ kindles): add the following to /etc/fstab:

,x-systemd.device-timeout=1ms

#kindles (force full access to)
UUID=3A50-7153 /home/j/mnt vfat defaults,auto,nofail,x-systemd.device-timeout=1ms,uid=j,gid=j,umask=000  0  0
UUID=5C3E-2D1C /home/j/mnt vfat defaults,auto,nofail,x-systemd.device-timeout=1ms,uid=j,gid=j,umask=000  0  0

note: defaults,auto,nofail.. are mount options. you can also use them in terminal: sudo mount /dev/sdb1 ~/mnt -o defaults,uid=j,gid=j,umask=0022
note: sudo mount /dev/sdb1 ~/mnt -o uid=j,gid=j,umask=0022 will give you full rw access
note: umask=000 should give full access, (u|g)id forces the fs to be owned by j, auto enables automatic mounting on boot (and seemingly also on plugging in), nofail suppresses error msg-s when mounting fails, timeout=1ms should move quickly on if mounting fails (probably redundant), defaults: use default options: rw, suid, dev, exec, auto, nouser, async, and relatime (so defaults,auto is redundant)
note: atm i'm using different mount points for kindles, as the 2nd one (..2D1C) failed to mount w/ a shared mount point. it may be the case that (at least auto) req-es a unique mountpoint per uuid (and w/ a shared mountpoint, if mounting the first uuid fails then all fail)

manually mount them w/ one of

sudo mount /dev/sdX1 mnt -o (uid=j -o gid=j -o umask=000|uid=j,gid=j,umask=000)
sudo mount -a : mount all fs-s per /etc/fstab

note: uid and gid flags only work on filesystems that don't support Linux permissions schema - ie, FAT and NTFS, among others. See askubuntu.com/a/34068/329506 
note0: "sudo mount /home/j/mnt" doesn't work (while it should by https://wiki.archlinux.org/index.php/Fstab)

https://serverfault.com/questions/39288/how-to-change-owner-of-mount-point/185856#comment929011_382566
https://linux.die.net/man/8/mount

-mount shows no error but doesn't mount:-

possible cause: your mount point is (named as) a sys dir, e.g. "mnt" or "lib"

-90 sec (unsuccessful) job running on boot:-

the likely cause is /etc/fstab. compare the contents of this file w/ the output of "sudo blkid" and correct the wrong data (e.g. uuids) in the file (or comment out the devices you're not using)
